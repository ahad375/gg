# gg

[![CI Status](http://img.shields.io/travis/ahad11/gg.svg?style=flat)](https://travis-ci.org/ahad11/gg)
[![Version](https://img.shields.io/cocoapods/v/gg.svg?style=flat)](http://cocoapods.org/pods/gg)
[![License](https://img.shields.io/cocoapods/l/gg.svg?style=flat)](http://cocoapods.org/pods/gg)
[![Platform](https://img.shields.io/cocoapods/p/gg.svg?style=flat)](http://cocoapods.org/pods/gg)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

gg is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'gg'
```

## Author

ahad11, aalarifi64@gmail.com

## License

gg is available under the MIT license. See the LICENSE file for more info.
